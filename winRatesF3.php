<?php
require_once 'clases/conexion.php';
// Traigo el id de los mazos seleccionados para mostrar los winRates
if($_POST['idMazo'] != "0"){
$deck = $_POST['idMazo'];

settype($deck, 'int');

// Traigo el mazo seleccionado

$queryDeckSelect = "SELECT * FROM decks WHERE id = $deck";
$traerDeckSelect = $conexion->query($queryDeckSelect);
$deckSelect = mysqli_fetch_assoc( $traerDeckSelect );


//Consulto todos los mazos del formato 3

$queryF3 = "SELECT * FROM decks WHERE formato_id = 3 ORDER BY cantDeUso DESC";
$traerDecks = $conexion->query($queryF3);

while( $mostrarDecks = mysqli_fetch_assoc( $traerDecks )){
$decks[] = $mostrarDecks;
}


//Consulto los datos de la tabla WinratesF3

$queryTablaWinRates = "SELECT *
from INFORMATION_SCHEMA.COLUMNS
where TABLE_SCHEMA = 'proyectodecks'
and TABLE_NAME = 'winRatesXMatchF3'";

$traerColumnsWinRates = $conexion->query($queryTablaWinRates);

while( $mostrarColumnsWinRates = mysqli_fetch_assoc( $traerColumnsWinRates )){
$columnsWinRates[] = $mostrarColumnsWinRates["COLUMN_NAME"];
}

//Consulto los winrates del formato 3

$queryWinRatesF3 = "SELECT * FROM winRatesXMatchF3 Where id_mazo = $deckSelect[id]";
$traerWinRates = $conexion->query($queryWinRatesF3);

while( $mostrarWinRates = mysqli_fetch_assoc( $traerWinRates )){
    $WinRates[] = $mostrarWinRates;
}



//Quito los primeros elementos(ID,MAZO_ID) para poder usarlos como posicion en los array de win Rates

array_shift($columnsWinRates);
array_shift($columnsWinRates);
echo"<br>";

foreach($columnsWinRates as $columnas){
    echo"<label>".$deckSelect['nombre']." ".$columnas."</label><br>";
    foreach ($WinRates as $rates) {
        echo"<input id='".$columnas."' idcolumna='".$columnas."' iddeck='".$deckSelect['id']."' value='".$rates[$columnas]."'> <br><br>";
    }

    echo"<script>

    function actualizarWinRate(idDeck, columna, valor){

        $.ajax({

            url:'actualizarF3.php',
            method: 'POST',
            data: {idDeck: idDeck, columna: columna, valor: valor},
            success: function(data){
            }
        })

    }

    $(document).on('blur', 'input#".$columnas."', function(){
    
        var columna = $(this).attr('id');
        var idDeck = $(this).attr('iddeck');
        var valorACambiar = $('input#".$columnas."').val();
        
        actualizarWinRate(idDeck, columna, valorACambiar);
        
    })
    
    </script>";

}

 


}





