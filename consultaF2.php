<?php 
require_once 'clases/conexion.php';

echo"FORMATO 2";

//Consulto todos los mazos del formato 2

$queryF2 = "SELECT * FROM decks WHERE formato_id = 2 ORDER BY cantDeUso DESC";
$traerDecks = $conexion->query($queryF2);

while( $mostrarDecks = mysqli_fetch_assoc( $traerDecks )){
$decks[] = $mostrarDecks;
}

//Consulto los datos de la tabla WinratesF2 para usarlos en la tabla winrates de php

$queryTablaWinRates = "SELECT *
from INFORMATION_SCHEMA.COLUMNS
where TABLE_SCHEMA = 'proyectodecks'
and TABLE_NAME = 'winRatesXMatchF2'";

$traerColumnsWinRates = $conexion->query($queryTablaWinRates);

while( $mostrarColumnsWinRates = mysqli_fetch_assoc( $traerColumnsWinRates )){
$columnsWinRates[] = $mostrarColumnsWinRates["COLUMN_NAME"];
}

//Consulto los winrates del formato 2

$queryWinRatesF2 = "SELECT * FROM winRatesXMatchF2";
$traerWinRates = $conexion->query($queryWinRatesF2);

while( $mostrarWinRates = mysqli_fetch_assoc( $traerWinRates )){
    $WinRates[] = $mostrarWinRates;
}


    // Crea la tabla de METAgame

echo "<br> Metagame <br>
    <table border='1px' style='text-align: center';>  
        <tr>
            <th>IDmaz</th>
            <th>Mazo</th>
            <th>Fecha de creacion</th>
            <th>Cantidad de uso</th>
            <th>WinRateGeneral</th>
            <th>Porcentaje de Uso</th>
        </tr>
";

//Creo una variable donde guardo la cantidad de partidas de todos los mazos

$cantidadPartidas = 0;
foreach($decks as $partidas){
    settype($partidas['cantDeUso'], 'int');
    $cantidadPartidas = $cantidadPartidas+$partidas['cantDeUso'];
}


foreach ($decks as $deck) {
    settype($deck['cantDeUso'], 'float');
    $porcentajeDeUso = ($deck['cantDeUso']*100)/$cantidadPartidas;
    $porcentajeDeUso = round($porcentajeDeUso,2);
    echo "
        <tr>
            <td>".$deck['id']." </td>
            <td>".$deck['nombre']." </td>
            <td>".$deck['fecha']." </td>
            <td>".$deck['cantDeUso']." </td>
            <td>".$deck['winRateGeneral']." </td>
            <td>".$porcentajeDeUso."%"."</td>
        </tr>
    ";
}

echo"<tr>
        <td>Total partidas:</td>
        <td>".$cantidadPartidas."</td>
        <td>Porcentaje de uso:</td>
        <td>100%</td>
    </tr>
    </table>
";



echo "<br> MetaWinRate <br><table border='1px' style='text-align: center';>";
echo "<tr>";
foreach($columnsWinRates as $columnas){

    echo"<td>".$columnas."</td>";
}
echo "</tr>";

//Quito los primeros elementos(ID,MAZO_ID) para poder usarlos como posicion en los array de win Rates

array_shift($columnsWinRates);
array_shift($columnsWinRates);

//Crea la tabla de winRates 

foreach($WinRates as $winRatesXMatchup){
    echo"<tr>
        <td>".$winRatesXMatchup['id']."</td>
        <td>".$winRatesXMatchup['id_mazo']."</td>";
        foreach($columnsWinRates as $posicionColumn){
        echo"<td>".$winRatesXMatchup[$posicionColumn]."</td>";}
    echo"</tr>";
}
echo"</table>";

echo"<br><br><br>";

//Selecionadores de mazo

echo"<div>
<select name='formato2' id='formato2'>
    <option id='0' value='0'> Seleccione un mazo </option>";

    foreach($decks as $deck){
         echo"<option value='".$deck['id']."' id=".$deck['id'].">".$deck['nombre']."</option>";
    }
    echo"</select>VS
";
echo"<select name='formato2maz2' id='formato2maz2'>
<option id='0' value='0'> Seleccione un mazo </option>";

foreach($decks as $deck){
    echo"<option value='VS".$deck['nombre']."' id=VS".$deck['nombre'].">".$deck['nombre']."</option>";
}

echo" </select></div>";

echo"<div id='winRates'></div>";
echo"<script src='clases/winRateConsultas.js'></script>";

