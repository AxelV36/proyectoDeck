<?php 
require_once "conexion.php";

    //Consulta para traer los formatos

    $queryFormatos = "SELECT * FROM formatos";

    $buscarFormatos = $conexion->query($queryFormatos);
    

    while( $formato = mysqli_fetch_assoc( $buscarFormatos )){
    $conjuntoFormatos[] = $formato;
    }

?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <title>Document</title>
</head>
<body> 
        Eliga un formato:
            <br>
        <?php foreach($conjuntoFormatos as $formato):?>
            <br>
            <div>
        <a href="crearDeckF<?=$formato['id']?>.php" id="f<?=$formato['id']?>"> <?=$formato['nombre']?> </a>
        </div>
        <br>
        <?php endforeach ?>
            <br><br><br>
            <a href="../index.php"> Volver al inicio </a>

        </body>

</html>