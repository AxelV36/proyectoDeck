<?php

require "conexion.php";

$consulta1 = "CREATE SCHEMA `proyectodecks2`";

$consulta2 = "CREATE TABLE `proyectodecks2`.`decks` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `nombre` varchar(100) NOT NULL,
    `fecha` date DEFAULT '2020-04-01',
    `cantDeUso` bigint(250) DEFAULT NULL,
    `winRateGeneral` varchar(100) DEFAULT '50.00%',
    `formato_id` int(11) DEFAULT NULL,
    PRIMARY KEY (`id`)
  );";

  $consulta3 = "CREATE TABLE `proyectodecks2`.`formatos` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `nombre` varchar(100) NOT NULL,
    `fecha` date DEFAULT NULL,
    PRIMARY KEY (`id`)
  );
  ";
  


  $consulta5 = "CREATE TABLE `proyectodecks2`.`winratesxmatchf1` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `id_mazo` int(11) NOT NULL,
    PRIMARY KEY (`id`)
  );
  ";

  $consulta6 = "CREATE TABLE `proyectodecks2`.`winratesxmatchf2` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `id_mazo` int(11) NOT NULL,
    PRIMARY KEY (`id`)
  );
  ";

  $consulta7= "CREATE TABLE `proyectodecks2`.`winratesxmatchf3` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `id_mazo` int(11) NOT NULL,
    PRIMARY KEY (`id`)
  );";
  
  $consulta8 = "CREATE TABLE `proyectodecks2`.`winratesxmatchf4` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `id_mazo` int(11) NOT NULL,
    PRIMARY KEY (`id`)
  );
  ";
  
  $ejecutar1 = $conexion->query($consulta1);
  $ejecutar2 = $conexion->query($consulta2);
  $ejecutar3 = $conexion->query($consulta3);
 
  $ejecutar5 = $conexion->query($consulta5);
  $ejecutar6 = $conexion->query($consulta6);
  $ejecutar7 = $conexion->query($consulta7);
  $ejecutar8 = $conexion->query($consulta8);

  if (!$ejecutar1) {
    printf("Errormessage: %s\n", $conexion->error);
 }

 if (!$ejecutar2) {
    printf("Errormessage: %s\n", $conexion->error);
 }

if (!$ejecutar3) {
   printf("Errormessage: %s\n", $conexion->error);
}



 if (!$ejecutar5) {
    printf("Errormessage: %s\n", $conexion->error);
 }

if (!$ejecutar6) {
   printf("Errormessage: %s\n", $conexion->error);
}

if (!$ejecutar7) {
    printf("Errormessage: %s\n", $conexion->error);
 }
 if (!$ejecutar8) {
    printf("Errormessage: %s\n", $conexion->error);
 }


 $consultaF1 = "INSERT into `proyectodecks2`.`formatos` (id, nombre) values (1, 'formato1');";
 $consultaF2 = "INSERT into `proyectodecks2`.`formatos` (id, nombre) values (2, 'formato2');";
 $consultaF3 = "INSERT into `proyectodecks2`.`formatos` (id, nombre) values (3, 'formato3');";
 $consultaF4 = "INSERT into `proyectodecks2`.`formatos` (id, nombre) values (4, 'formato4');";

 $ejeF1 = $conexion->query($consultaF1);
 $ejeF2 = $conexion->query($consultaF2);
 $ejeF3 = $conexion->query($consultaF3);
 $ejeF4 = $conexion->query($consultaF4);

 if (!$ejeF1) {
    printf("Errormessage: %s\n", $conexion->error);
 }
 if (!$ejeF2) {
    printf("Errormessage: %s\n", $conexion->error);
 }
 if (!$ejeF3) {
    printf("Errormessage: %s\n", $conexion->error);
 }
 if (!$ejeF4) {
    printf("Errormessage: %s\n", $conexion->error);
 }