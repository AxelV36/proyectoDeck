
$(document).ready(function(){

    function refrescarDecks(){
        location.reload(); 
      }

    function obtenerDecks(){

        $.ajax({
        
        url: 'mostrarDeckF3.php',
        method: 'POST',
        succes: function(data){
        $("#traerdecks").html(data);
        }
        })
        }
        
        obtenerDecks();
        

  $(document).on('click', "#add", function(){

    var idDeck = $("#addId").text();
      var nombre = $("#addNombre").text();
      var fecha = $("#addFecha").text();
      var cantUso = $("#addUso").text();
      var winRate = $("#addWinRate").text();
      var idFormato = 3;
      
      $.ajax({
          url: 'insertarDeckF3.php',
          method: 'POST',
          data:{id: idDeck, nombre: nombre, fecha: fecha, cantUso: cantUso, winRate: winRate, idFormato: idFormato},
          succes: function(data){
              refrescarDecks();
          }
      })

      

  })

  $(document).on('click', "#eliminar", function(){
    if(confirm("Quieres eleminar este Deck?")){
      var deckId = $(this).attr('deckid');
      var deckName = $(this).attr('deckName');


      $.ajax({

          url: 'borrarDeckF3.php',
          method: 'POST',
          data:{idDeck: deckId, deckName: deckName},
          succes: function(data){
              refrescarDecks();
          }

      })
    }
})

 

  $(document).on('click', "#traer", function(){

        refrescarDecks();


  })

})
